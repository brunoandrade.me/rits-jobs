@extends('layouts.app')

@section('title') Página Inicial @endsection

@section('content')
    @include('includes.about')
    @include('includes.jobs')
@endsection
