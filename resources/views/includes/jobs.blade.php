<section class="jobs">

    <h1 class="jobs__title">Vagas em Aberto</h1>
    <p class="jobs__description">Conheça as oportunidades que temos em aberto.</p>

    <div class="jobs-cards">
        <div class="j-cards_card c-card-jobs">
            <h5 class="j-cards_single-title">Desenvolvedor Front-end</h5>
            <p class="j-cards_single-local">Natal - RN, Brasil</p>
            <a class="j-cards__single-btn btn btn-primary" href="#" target="_blank">Candidate-se</a>
        </div><!-- / .j-cards_card .c-card-jobs-->

        <div class="j-cards_card c-card-jobs">
            <h5 class="j-cards_single-title">Desenvolvedor Front-end</h5>
            <p class="j-cards_single-local">Natal - RN, Brasil</p>
            <a class="j-cards__single-btn btn btn-primary" href="#" target="_blank">Candidate-se</a>
        </div><!-- / .j-cards_card .c-card-jobs-->

        <div class="j-cards_card c-card-jobs">
            <h5 class="j-cards_single-title">Desenvolvedor Front-end</h5>
            <p class="j-cards_single-local">Natal - RN, Brasil</p>
            <a class="j-cards__single-btn btn btn-primary" href="#" target="_blank">Candidate-se</a>
        </div><!-- / .j-cards_card .c-card-jobs-->

    </div> <!-- / .jobs-cards-->
</section><!-- / .jobs-->