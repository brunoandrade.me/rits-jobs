<section class="about">
    <div class="about__section-left">
        <h1> A rits </h1>
        <p>
            A Rits é uma empresa focada no desenvolvimento de produtos digitais, como aplicativos, sistemas em nuvem e websites de alta complexidade e no outsourcing de TI, com equipes preparadas para atender todas as necessidades da sua empresa. Usamos a metodologia ágil e provamos que inteligência de negócio, alta qualidade e entrega rápida podem caminhar juntos. Nosso objetivo é um só: tirar o seu projeto digital do papel. <br> Somos uma empresa de desenvolvimento de softwares, mas vamos além: trabalhamos com consultoria de negócios para oferecer as melhores possibilidades de transformar seus projetos em realidade.
        </p>
        <a class="l-btn-secondary c-btn-secondary" href="#">Sobre a gente</a>
        <a class="l-btn-primary c-btn-primary" href="#">Confira as vagas</a>
    </div> <!-- / .about__section-left-->

    <div class="about__section-right">
        <div class="box-text">
            <h2>Para Entregar com <span>alta qualidade</span> e rapidez, trabalhamos com <span>inteligência e metodologias ágeis de desenvolvimento.</span></h2>
        </div> <!-- / .box-text-->
    </div> <!-- / .about__section-right-->

</section> <!-- / .about-->

<div class="our-values__images">
        <img src="{{ asset('img/image-about.jpg') }}" alt="Para o Alto e Avante">
    </div>

<section class="our-values">

    <h1>Nossos valores</h1>
    <div class="values">
        <span class="values__text-one">NUNCA DEIXE A EQUIPE NA MÃO</span>
        <h3 class="values__text-two">“All for one and one for all, united we stand divided we fall.”</h3>
        <p class="values__text-author">Alexandre Dumas, The Three Musketeers</p>
    </div><!-- / .values -->

    <div class="values">
        <span class="values__text-one">OLHOS NA TAREFA E CABEÇA NA SOLUÇÃO</span>
        <h3 class="values__text-two">“Sword Of Omens, give me sight beyond sight.”</h3>
        <p class="values__text-author">Buzz Lightyear, Toy Story</p>
    </div><!-- / .values -->

    <div class="values">
        <span class="values__text-one">FAÇA ACONTECER</span>
        <h3 class="values__text-two">“Do. Or do not. There is no try.”</h3>
        <p class="values__text-author">Master Yoda, The Empire Strikes Back</p>
    </div><!-- / .values -->

    <div class="values">
        <span class="values__text-one">FALHE RÁPIDO, MELHORE MAIS RÁPIDO AINDA</span>
        <h3 class="values__text-two">“To infinity… and beyond!”</h3>
        <p class="values__text-author">Buzz Lightyear, Toy Story</p>
    </div><!-- / .values -->

    <div class="values">
        <span class="values__text-one">SAIBA OU APRENDA</span>
        <h3 class="values__text-two">“Be water, my friend.”</h3>
        <p class="values__text-author">Bruce Lee</p>
    </div><!-- / .values -->

</section><!-- / .our-values -->