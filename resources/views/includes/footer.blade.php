
<footer class="footer">
    <img src="" alt="Rits Tecnologia">
    <p class="footer__copyright">Rits Tecnologia. Todos os direitos reservados.</p>
    <p class="footer__description">Desenvolver e evoluir soluções digitais para negócios que acreditam na tecnologia como força propulsora.</p>
    <a class="footer__link-site" href="#">Rits.com.br</a>
</footer>

@include('includes.scripts')