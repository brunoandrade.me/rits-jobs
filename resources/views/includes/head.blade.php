
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="theme-color" content="#563C97" >
        <meta name="description" content="Somos uma equipe multidisciplinar que desenvolve soluções personalizadas em tecnologia da informação usando metodologias ágeis de desenvolvimento."/>
        <meta name="keywords" content="tecnologia,ti,projetos,produtos,cloud,nuvem,outsourcing, assinaturas, recorrência"/>       
        <link rel="icon" href="https://rits.com.br/app/uploads/2017/05/rits-favicon.png" sizes="32x32" />
        <link rel="icon" href="https://rits.com.br/app/uploads/2017/05/rits-favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="https://rits.com.br/app/uploads/2017/05/rits-favicon.png" />
        <meta name="msapplication-TileImage" content="https://rits.com.br/app/uploads/2017/05/rits-favicon.png" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}" media="all" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>

 