<header class="header">


        <nav class="nav-menu">
            <img class="nav-menu__logo-rits" src="{{ asset('img/logo_rits.svg') }}" alt="Logo Rits">
            <ul class="nav-menu__list">
                <li class="nav-menu__item"><a class="nav-menu__link" href="#">A Rits</a></li>
                <li class="nav-menu__item"><a class="nav-menu__link" href="#">Nossos Valores</a></li>
                <li class="nav-menu__item"><a class="nav-menu__link c-btn-primary" href="#">Vagas Abertas</a></li>
            </ul> <!-- / .nav-menu__list -->
        </nav> <!-- / .nav-menu__options -->

    {{-- 
        Esta parte do cabeçalho só deve ser exibida
        caso esteja na página inicial 
    --}}

    @if (Route::has('home'))

    <section class="h-intro">
        <h1 class="h-intro__title">Venha fazer <span>parte</span> do nosso <span>time</span>!</h1>
        <p class="h-intro__text">Se você gosta de estar em constante aprendizado, trabalhar em equipe e está sempre disposto a contribuir com melhorias nos projetos que participa, temos uma oportunidade para você.</p>

        <a class="h-intro__btn-secondary c-btn-secondary" href="#">Sobre a gente</a>
        <a class="h-intro__btn-primary c-btn-primary" href="">Confira as vagas</a>
    </section><!-- / .h-intro -->

    @endif

    </header><!-- / .header -->
    

   
