<?php

Route::get('/', function () {
    return view('pages.home');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');
